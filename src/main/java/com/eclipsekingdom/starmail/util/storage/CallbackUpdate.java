package com.eclipsekingdom.starmail.util.storage;

public interface CallbackUpdate {

    void onUpdateDone();

}
