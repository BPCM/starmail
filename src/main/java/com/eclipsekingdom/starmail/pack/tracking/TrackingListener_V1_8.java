package com.eclipsekingdom.starmail.pack.tracking;

import com.eclipsekingdom.starmail.StarMail;
import com.eclipsekingdom.starmail.pack.Pack;
import com.eclipsekingdom.starmail.warehouse.WarehouseCache;
import org.bukkit.Bukkit;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.ItemDespawnEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TrackingListener_V1_8 implements Listener {

    public TrackingListener_V1_8() {
        Plugin plugin = StarMail.getPlugin();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }
    @EventHandler
    public void onPickup(PlayerPickupItemEvent e) {
        UUID itemID = e.getItem().getUniqueId();
        if (TrackingRunnable.sealedItems.contains(itemID)) {
            TrackingRunnable.sealedItems.remove(itemID);
        }
    }


}